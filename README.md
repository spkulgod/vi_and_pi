#### Description:

This project presents an approach of implementing Value Iteration (VI) and Policy Iteration (PI) to obtain an optimal control policy for Balancing an Inverted Pendulum. The discretization of the statespace,controlspcae and time is done so that is suitable for finding the optimal control using  VI and PI. The parameters are varied to obtain a set that guarantee convergence to the optimal value function, and the convergence is compared between VI and PI. Interpolation is used to extend the control policy to continuous space and several trajectories are simulated for the continuous system, starting at different initial states and having different noise values to compare the performance of the control policy obtained. 

#### Results:  

##### Optimal policy obtained:

###### Optimal Value:
![Alt text](gifs/figure_1.png "optimal value")

###### Optimal Policy:
![Alt text](gifs/figure_2.png "optimal policy")

##### Result visualization:
Initial theta 180 degrees, 0 velocity and sigma 0.1.  
![Alt text](gifs/p180v0s0.1.gif "gif1")

Initial theta 180 degrees, 0 velocity and sigma 0.01.   
![Alt text](gifs/p180v0s0.01.gif "gif2")

Initial theta 180 degrees, 1 velocity and sigma 0.1.   
![Alt text](gifs/p180v1s0.1.gif "gif2")

