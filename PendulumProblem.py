"""
==================================
Inverted pendulum animation class
==================================

Adapted from the double pendulum problem animation.
https://matplotlib.org/examples/animation/double_pendulum_animated.html
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import math
import itertools
from scipy.stats import multivariate_normal
import copy
np.set_printoptions(threshold=np.inf,linewidth=300)

class Dynamics:
    def __init__(self):
        self.a= 1
        self.b= 0.01
        s = 0.1
        sigma = s*np.eye(2)
        self.k = 1
        self.r = 0.01
        self.gamma = 0.9
        self.dt = 0.05
        self.n1 = 301
        self.n2 = 31
        self.nu = 11
        self.vmax = 3
        self.umax = 6
        self.thmin = 0
        self.xmin = np.array([self.thmin,-self.vmax])
        self.resx = np.array([2*np.pi/(self.n1-1),2*self.vmax/(self.n2-1)])
        self.resu = 2*self.umax/(self.nu-1)
        self.ssqdt = sigma*sigma.T*self.dt
        self.threesig = s*np.array([3,3])*math.sqrt(self.dt) 
        print(self.threesig)

    def stage_cost(self,x,u):
        return 1-np.exp(self.k*math.cos(x[0])-self.k)+self.r/2*u*u
    
    def next_states(self,x,u):
        fxu = np.array([x[1],self.a*math.sin(x[0])-self.b*x[1]+u])
        x_mean = x+fxu*self.dt
        low_grid_x = ((x_mean-self.threesig-self.xmin)/self.resx).astype(int)
        high_grid_x = ((x_mean+self.threesig-self.xmin)/self.resx).astype(int)
        temp = [np.arange(low_grid_x[0],high_grid_x[0]+1),
            np.arange(low_grid_x[1],high_grid_x[1]+1)]
        #print(temp)
        x_grid = np.array(list(itertools.product(*temp)))
        #print(low_grid_x,high_grid_x)
        #print(x_grid)
        x_pos = x_grid*self.resx+self.xmin
        x_pos[:,1][x_pos[:,1]<=-self.vmax] = -self.vmax
        x_pos[:,1][x_pos[:,1]>=self.vmax] = self.vmax
        norm = multivariate_normal(mean=x_mean,cov=self.ssqdt)
        prob = norm.pdf(x_pos)
        prob = prob/np.sum(prob)
        x_pos = x_pos[prob>0.01]
        prob = prob[prob>0.01]
        prob = prob/np.sum(prob)
        x_pos[:,0] = x_pos[:,0]%(2*np.pi)
        x_grid = ((x_pos-self.xmin)/self.resx).astype(int)
        return x_grid,prob

class Iterations:
    def __init__(self):
        self.dyn = Dynamics()
        self.prob_matrix = np.zeros((self.dyn.n1,self.dyn.n2,self.dyn.nu),dtype=object)
        self.tran_matrix = np.zeros((self.dyn.n1,self.dyn.n2,self.dyn.nu),dtype=object)
        self.l = np.zeros((self.dyn.n1,self.dyn.nu))
        for i in range(self.dyn.n1):
            print(i)
            for j in range(self.dyn.n2):
                x_grid= np.array([i,j])
                x = x_grid*self.dyn.resx+self.dyn.xmin
                for u in range(self.dyn.nu):
                    u_val = u*self.dyn.resu-self.dyn.umax
                    x_dash,p = self.dyn.next_states(x,u_val)
                    #print([i,j,u])
                    #print(x,u_val)
                    #print(x_dash)
                    self.prob_matrix[i,j,u] = p
                    self.tran_matrix[i,j,u] = x_dash
                    self.l[i,u] = self.dyn.stage_cost(x,u_val)
        np.save('tran_prob',self.prob_matrix)
        np.save('tran_x',self.tran_matrix)
        print("done")

    def value_it(self,epsilon,gamma):
        V = np.ones((self.dyn.n1,self.dyn.n2))
        pi = np.zeros((self.dyn.n1,self.dyn.n2))
        Vdash = np.zeros((self.dyn.n1,self.dyn.n2))
        while np.max(np.abs(V-Vdash))>epsilon:
            V = copy.deepcopy(Vdash)
            for theta in range(self.dyn.n1):
                #print(theta)
                for omega in range(self.dyn.n2):
                    Q_vec = []
                    #x_grid= np.array([theta,omega])
                    #x = x_grid*self.dyn.resx+self.dyn.xmin
                    for u in range(self.dyn.nu):
                        #print(theta,omega,u)
                        #sum = 0
                        l = self.l[theta,u]
                        #x_dash,p = self.dyn.next_states(x,u)
                        x_dash = self.tran_matrix[theta,omega,u]
                        p= self.prob_matrix[theta,omega,u]
                        #for i in range(len(p)):
                        #    sum+= V[tuple(x_dash[i])]*p[i]
                        sum = np.sum(V[tuple([x_dash[:,0],x_dash[:,1]])]*p)
                        Q_vec.append(l+gamma*sum)
                    #print(Q_vec)
                    Vdash[theta,omega] = min(Q_vec)
                    pi[theta,omega] = np.argmin(Q_vec)
            print(np.max(np.abs(V-Vdash)))
        np.save("V",Vdash)
        np.save("pi",pi)
        plt.imshow(Vdash)
        plt.colorbar()
        plt.show()

    def policy_eval(self,V,pi,epsilon,gamma):
        V = np.ones((self.dyn.n1,self.dyn.n2))
        Vdash = np.zeros((self.dyn.n1,self.dyn.n2))
        while np.max(np.abs(V-Vdash))>epsilon:
            V = copy.deepcopy(Vdash)
            for theta in range(self.dyn.n1):
                #print(theta)
                for omega in range(self.dyn.n2):
                    u = int(pi[theta,omega])
                    l = self.l[theta,u]
                    x_dash = self.tran_matrix[theta,omega,u]
                    p= self.prob_matrix[theta,omega,u]
                    sum = np.sum(V[tuple([x_dash[:,0],x_dash[:,1]])]*p)
                    Vdash[theta,omega] = l+gamma*sum
            print(np.max(np.abs(V-Vdash)))
        return Vdash

    def policy_imp(self,V,pi,gamma):
        for theta in range(self.dyn.n1):
            for omega in range(self.dyn.n2):
                Q_vec = []
                for u in range(self.dyn.nu):
                    l = self.l[theta,u]
                    x_dash = self.tran_matrix[theta,omega,u]
                    p= self.prob_matrix[theta,omega,u]
                    sum = np.sum(V[tuple([x_dash[:,0],x_dash[:,1]])]*p)
                    Q_vec.append(l+gamma*sum)
                pi[theta,omega] = np.argmin(Q_vec)
    
    def policy_it(self,epsilon,gamma):
        V = np.ones((self.dyn.n1,self.dyn.n2))
        pi = np.zeros((self.dyn.n1,self.dyn.n2))
        Vdash = np.zeros((self.dyn.n1,self.dyn.n2))
        while np.max(np.abs(V-Vdash))>0:
            V = copy.deepcopy(Vdash)
            Vdash = self.policy_eval(copy.deepcopy(V),pi,epsilon,gamma)
            self.policy_imp(V,pi,gamma)
        np.save("V_PI",Vdash)
        np.save("pi_PI",pi)
        plt.imshow(Vdash)
        plt.colorbar()
        plt.show()

class EnvAnimate:

    '''
    Initialize Inverted Pendulum
    '''
    def __init__(self):

        # Change this to match your discretization
        self.dt = 0.05
        self.t = np.arange(0.0, 20.0, self.dt)

        # Random trajectory for example
        self.theta = np.zeros(self.t.shape[0])
        self.omega = np.zeros(self.t.shape[0])
        self.x1 = np.sin(self.theta)
        self.y1 = np.cos(self.theta)
        self.u = np.zeros(self.t.shape[0])
        self.x_prev = [0,0]

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111,autoscale_on=False, xlim=(-2,2), ylim=(-2,2))
        self.ax.grid()
        self.ax.axis('equal')
        plt.axis([-2, 2, -2, 2])

        self.line, = self.ax.plot([],[], 'o-', lw=2)
        self.time_template = 'time = %.1fs \nangle = %.2frad\ncontrol = %.2f'
        self.time_text = self.ax.text(0.05, 0.8, '', transform=self.ax.transAxes)
        self.pi = np.load("pi.npy")
        self.tran_x = np.load('tran_x.npy',allow_pickle = True)
        self.tran_prob = np.load('tran_prob.npy',allow_pickle = True)
        self.dyn = Dynamics()

    def sample_next_state(self,x,u):
        print(x)
        x_grid = ((x-self.dyn.xmin)/self.dyn.resx).astype(int)
        print(x_grid)
        x_dash = self.tran_x[x_grid[0],x_grid[1],u]
        p= self.tran_prob[x_grid[0],x_grid[1],u]
        pick = np.random.rand()
        p_cum = np.cumsum(p)
        val = np.where(p_cum>=pick)[0][0]
        return x_dash[val]

    '''
    Provide new rollout theta values to reanimate
    '''
    def new_data(self, theta):
        self.theta = theta
        self.x1 = np.sin(theta)
        self.y1 = np.cos(theta)
        self.u = np.zeros(self.t.shape[0])

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111,autoscale_on=False, xlim=(-2,2), ylim=(-2,2))
        self.ax.grid()
        self.ax.axis('equal')
        plt.axis([-2, 2,-2, 2])
        self.line, = self.ax.plot([],[], 'o-', lw=2)
        self.time_template = 'time = %.1fs \nangle = %.2frad\ncontrol = %.2f'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)

    def init(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def _update(self, i):
        u=int(self.pi[self.x_prev[0],self.x_prev[1]])
        print(u)
        x_next = self.sample_next_state([self.theta[i],self.omega[i]],u)
        self.x_prev = x_next
        print(self.theta[i])
        x = x_next*self.dyn.resx+self.dyn.xmin
        self.theta[i+1] = x[0]
        self.omega[i+1] = x[1]
        x1 = np.sin(x[0])
        y1 = np.cos(x[0])
        thisx = [0, x1]
        thisy = [0, y1]
        self.line.set_data(thisx, thisy)
        self.time_text.set_text(self.time_template % (self.t[i], self.theta[i], self.u[i]))
        return self.line, self.time_text

    def start(self):
        print('Starting Animation')
        print()
        # Set up plot to call animate() function periodically
        self.ani = FuncAnimation(self.fig, self._update, frames=range(len(self.x1)), interval=25, blit=True, init_func=self.init, repeat=False)
        plt.show()


if __name__ == '__main__':

    '''dynamics = Dynamics()
    x_grid=[8, 0]
    x = x_grid*dynamics.resx+dynamics.xmin
    u = 8
    u_val = u*dynamics.resu-dynamics.umax
    print(x,u_val)
    l,y = dynamics.next_states(x,u_val)
    l = l*dynamics.resx+dynamics.xmin'
    #print(l,y)
    #print(dynamics.stage_cost([1,0.4],0))'''
    it = Iterations()
    #it.value_it(epsilon=0.0001,gamma=0.9)
    it.policy_it(epsilon=0.0001,gamma=0.9)
    pi=np.load("pi.npy")
    plt.imshow(pi)
    print(pi)
    plt.colorbar()
    plt.show()
    animation = EnvAnimate()
    animation.start()